package com.sweetgum.langdetect.detector;

import com.sweetgum.langdetect.classifier.NaiveBayesClassifier;
import com.sweetgum.langdetect.util.LangUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LangDetector {
public static Log LOG = LogFactory.getLog(LangDetector.class);
private Pattern NGRAM_PATTERN = Pattern.compile(".{3}");
private String fileName;
private int numIterations;
private List<List<String>> data = new ArrayList<List<String>>();
private NaiveBayesClassifier finalClassifier = new NaiveBayesClassifier();

public LangDetector(String fileName, int numIterations) throws Exception {
  this.fileName = fileName;
  this.numIterations = numIterations;
}

public LangDetector(String fileName, int numIterations, int ngramSize) throws Exception {
  this(fileName, numIterations);
  NGRAM_PATTERN = Pattern.compile(String.format(".{%d}", ngramSize));
}

public void loadData() throws Exception {
  FileReader fileReader = new FileReader(new File(fileName));
  BufferedReader bufferedReader = new BufferedReader(fileReader);

  String line;
  while ((line = bufferedReader.readLine()) != null) {
    data.add(findNGrams(line));
  }

  bufferedReader.close();
  fileReader.close();
}

public void test(String testFileName) throws Exception {
  FileReader fileReader = new FileReader(new File(testFileName));
  BufferedReader bufferedReader = new BufferedReader(fileReader);

  String line;
  while ((line = bufferedReader.readLine()) != null) {
    String lang = classify(line);
    LOG.info(String.format("%s --> %s", lang, line));
  }

  bufferedReader.close();
  fileReader.close();
}


public List<String> findNGrams(String text) {
  text = LangUtils.cleanUp(text);
  List<String> ngrams = new ArrayList<String>();
  Matcher matcher = NGRAM_PATTERN.matcher(text);
  while (matcher.find()) {
    ngrams.add(matcher.group());
  }
  return ngrams;
}

public void train() {
  NaiveBayesClassifier classifier = new NaiveBayesClassifier();
  finalClassifier = classifier.trainEM(numIterations, data);
  if (finalClassifier.getPriorCategoryProbability(0) > finalClassifier.getPriorCategoryProbability(1)) {
    finalClassifier.setCategoryNames(Arrays.asList(new String[]{"majority", "minority"}));
  } else {
    finalClassifier.setCategoryNames(Arrays.asList(new String[]{"minority", "majority"}));
  }
}

public String classify(String line) {
  Integer categoryIndex = finalClassifier.classify(findNGrams(line));
  return finalClassifier.getCategoryNames().get(categoryIndex);
}

public static void main(String args[]) {
  String fileName = "data/tweets-5000.txt";
  //String fileName = "data/gutenberg-training-en";
  String testFileName = "data/tweets-test.txt";
  try {
    LangDetector detector = new LangDetector(fileName, 50);
    detector.loadData();
    detector.train();
    detector.test(testFileName);
  } catch (Exception ex) {
    LOG.error("Exception", ex);
  }

}

}
