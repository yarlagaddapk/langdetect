package com.sweetgum.langdetect.util;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LangUtils {
private static Pattern REMOVE_HASHTAGS = Pattern.compile("#\\w+");
private static Pattern REMOVE_TWEETERS = Pattern.compile("@\\w+");
private static Pattern REMOVE_LINKS_HTTP = Pattern.compile("(http|https)://\\S+");
private static Pattern REMOVE_LINKS_WWW = Pattern.compile("www\\.\\S+");
private static Pattern REMOVE_LINKS_COM = Pattern.compile("\\S+\\.com");
private static Pattern REMOVE_NON_ALPHA_NUMERIC = Pattern.compile("[^a-zA-Z0-9\\s]");
private static Pattern BIGRAM_PATTERN = Pattern.compile(".{2}");


public static String removeHashTags(String tweet) {
  Matcher matcher = REMOVE_HASHTAGS.matcher(tweet);
  return matcher.replaceAll("");
}

public static String removeTweeters(String tweet) {
  Matcher matcher = REMOVE_TWEETERS.matcher(tweet);
  return matcher.replaceAll("");
}

public static String removeLinks(String tweet) {
  Matcher matcher = REMOVE_LINKS_HTTP.matcher(tweet);
  matcher = REMOVE_LINKS_WWW.matcher(matcher.replaceAll(""));
  matcher = REMOVE_LINKS_COM.matcher(matcher.replaceAll(""));
  return matcher.replaceAll("");
}

public static String removeNonAlphaNumeric(String tweet) {
  Matcher matcher = REMOVE_NON_ALPHA_NUMERIC.matcher(tweet);
  return matcher.replaceAll("");
}

public static String cleanUp(String tweet) {
  return removeNonAlphaNumeric(removeLinks(removeTweeters(removeHashTags(tweet)))).toLowerCase().trim();
}

public static List<String> findBiGrams(String tweet) {
  List<String> bigrams = new ArrayList<String>();
  Matcher matcher = BIGRAM_PATTERN.matcher(tweet);
  while(matcher.find()) {
    bigrams.add(matcher.group());
  }
  return bigrams;
}

public static List<String> generateNGramsForWord(String word, int n) {
  if (word == null || word.length() == 0) {
    return Collections.emptyList();
  }
  int origLen = word.length();
  List<String> ngrams = new ArrayList<String>();
  word = StringUtils.leftPad(word, word.length() + 1, "_");
  word = StringUtils.rightPad(word, word.length() + (n - 1), "_");
  for (int i = 0; i < origLen + 1; i++) {
    ngrams.add(word.substring(i, i + n));
  }
  return ngrams;
}

public static List<String> generateNGramsForSentence(String sentence, int n) {
  if (sentence == null || sentence.length() == 0) {
    return Collections.emptyList();
  }

  StringTokenizer tokenizer = new StringTokenizer(sentence);
  List<String> words = new ArrayList<String>();
  while (tokenizer.hasMoreTokens()) {
    words.add(tokenizer.nextToken());
  }
  if (words.isEmpty()) {
    return Collections.emptyList();
  }

  List<String> allNGrams = new ArrayList<String>();
  for (String word : words) {
    allNGrams.addAll(generateNGramsForWord(word, n));
  }
  return allNGrams;
}

}
