package com.sweetgum.langdetect.util;

import java.util.List;

public class MathUtils {

public static Double sum(List<Double> values) {
  Double sum = 0.0d;
  if (values == null) {
    return sum;
  }
  for (int i = 0; i < values.size(); i++) {
    sum += values.get(i);
  }
  return sum;
}

}
