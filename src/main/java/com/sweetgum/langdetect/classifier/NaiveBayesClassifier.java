package com.sweetgum.langdetect.classifier;

import com.sweetgum.langdetect.util.MathUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NaiveBayesClassifier {
private Integer numCategories = 2;
private Double priorTokenCount = 0.0001d;
private List<Double> priorCategoryCounts;
private List<String> categoryNames;
private Map<Integer, Map<String, Double>> tokenCounts;
private List<Double> totalTokenCounts;
private List<Double> categoryCounts;

public NaiveBayesClassifier() {
  this(2, 0.0001d);
}

public NaiveBayesClassifier(Integer numCategories, Double priorTokenCount) {
  this.numCategories = numCategories;
  this.priorTokenCount = priorTokenCount;
  this.priorCategoryCounts = new ArrayList<Double>(this.numCategories);
  tokenCounts = new HashMap<Integer, Map<String, Double>>();
  totalTokenCounts = new ArrayList<Double>(this.numCategories);
  categoryCounts = new ArrayList<Double>(this.numCategories);
  categoryNames = new ArrayList<String>(this.numCategories);
  for (int i = 0; i < this.numCategories; i++) {
    this.priorCategoryCounts.add(i, Math.random());
    this.totalTokenCounts.add(i, 0d);
    this.categoryCounts.add(i, 0d);
    this.categoryNames.add(i, String.format("Category%d", i));
  }
}

public void train(List<String> tokens, Integer categoryIndex, Double probability) {
  Map<String, Double> tokenCountsMap = tokenCounts.get(categoryIndex);
  if (tokenCountsMap == null) {
    tokenCountsMap = new HashMap<String, Double>();
    tokenCounts.put(categoryIndex, tokenCountsMap);
  }
  for (String token : tokens) {
    Double existingVal = tokenCountsMap.get(token);
    if (existingVal == null) {
      tokenCountsMap.put(token, probability);
    } else {
      tokenCountsMap.put(token, existingVal + probability);
    }
    totalTokenCounts.set(categoryIndex, totalTokenCounts.get(categoryIndex) + probability);
  }
  categoryCounts.set(categoryIndex, categoryCounts.get(categoryIndex) + probability);
}

public NaiveBayesClassifier trainEM(int numIterations, List<List<String>> trainingExamples) {
  NaiveBayesClassifier prevClassifier = new NaiveBayesClassifier();
  for (int i = 0; i < numIterations; i++) {
    NaiveBayesClassifier currClassifier = new NaiveBayesClassifier();

    for (List<String> example : trainingExamples) {
      List<Double> posteriorCategoryProbs = prevClassifier.getPosteriorCategoryProbabilities(example);

      for (int j = 0; j < this.numCategories; j++) {
        currClassifier.train(example, j, posteriorCategoryProbs.get(j));
      }
    }
    prevClassifier = currClassifier;
  }
  return prevClassifier;
}

public List<Double> getPosteriorCategoryProbabilities(List<String> tokens) {
  List<Double> unnormalizedPosteriorProbs = new ArrayList<Double>(this.numCategories);
  for (int i = 0; i < this.numCategories; i++) {
    Double prob = 0.0d;
    for (String token : tokens) {
      if (prob == 0.0d) {
        prob = getTokenProbability(token, i);
      } else {
        prob = prob * getTokenProbability(token, i);
      }
    }
    prob *= getPriorCategoryProbability(i);
    unnormalizedPosteriorProbs.add(i, prob);
  }
  Double normalized = MathUtils.sum(unnormalizedPosteriorProbs);
  if (normalized == 0.0d) {
    normalized = 1.0d;
  }
  for (int i = 0; i < this.numCategories; i++) {
    unnormalizedPosteriorProbs.set(i, unnormalizedPosteriorProbs.get(i) / normalized);
  }
  return unnormalizedPosteriorProbs;
}

public Double getTokenProbability(String token, Integer categoryIndex) {
  Double denom = totalTokenCounts.get(categoryIndex) + (tokenCounts.get(categoryIndex) == null ? 0 : tokenCounts.get(categoryIndex).size()) * priorTokenCount;
  if (denom == 0.0d) {
    return 0d;
  } else {
    Map<String, Double> tokenCountsMap = tokenCounts.get(categoryIndex);
    Double numer = priorTokenCount;
    if (tokenCountsMap != null && tokenCountsMap.get(token) != null) {
      numer += tokenCountsMap.get(token);
    }
    return (numer / denom);
  }
}

public Double getPriorCategoryProbability(Integer categoryIndex) {
  Double denom = MathUtils.sum(categoryCounts) + (1 * MathUtils.sum(priorCategoryCounts));
  if (denom == 0.0d) {
    return 0d;
  } else {
    return ((categoryCounts.get(categoryIndex) + (1 * priorCategoryCounts.get(categoryIndex)))) / denom;
  }
}

public void setCategoryNames(List<String> categoryNames) {
  this.categoryNames = categoryNames;
}

public List<String> getCategoryNames() {
  return this.categoryNames;
}

public Integer classify(List<String> tokens) {
  Double maxProb = -1d;
  Integer maxCategory = -1;

  if (tokens.isEmpty()) {
    for(int i=0; i<this.numCategories; i++) {
      Double priorProb = getPriorCategoryProbability(i);
      if (priorProb > maxProb) {
        maxCategory = i;
        maxProb = priorProb;
      }
    }
  } else {
    List<Double> posteriorCategoryProbs = getPosteriorCategoryProbabilities(tokens);
    for(int i=0; i<this.numCategories; i++) {
      if (posteriorCategoryProbs.get(i) > maxProb) {
        maxCategory = i;
        maxProb = posteriorCategoryProbs.get(i);
      }
    }
  }
  return maxCategory;
}

}
