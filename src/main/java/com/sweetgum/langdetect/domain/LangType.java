package com.sweetgum.langdetect.domain;

public enum LangType {
  eng,
  non_eng;

public static LangType getRandomLangType() {
  return LangType.values()[(int) (Math.random() * LangType.values().length)];
}
}
