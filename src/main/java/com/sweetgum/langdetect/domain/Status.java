package com.sweetgum.langdetect.domain;

public class Status {
private String text;
private LangType langType;
private Double prob;

public Status(String text, LangType langType) {
  this.text = text;
  this.langType = langType;
}

public String getText() {
  return text;
}

public LangType getLangType() {
  return langType;
}

public Double getProb() {
  return prob;
}

public void setProb(Double prob) {
  this.prob = prob;
}

@Override
public String toString() {
  return String.format("%s|%1.30f|%s", langType, prob, text);
}
}
