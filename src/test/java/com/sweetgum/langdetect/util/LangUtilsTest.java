package com.sweetgum.langdetect.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class LangUtilsTest {
private static Log LOG = LogFactory.getLog(LangUtilsTest.class);

@Test
public void generateNGramsForWordTest() {
  String word = "ab";
  List<String> expected = new ArrayList<String>();
  expected.add("_ab");
  expected.add("ab_");
  expected.add("b__");
  List<String> actual = LangUtils.generateNGramsForWord(word, 3);
  LOG.info(actual);
  Assert.assertTrue(expected.size() == actual.size());
}
}
